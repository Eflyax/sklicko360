<?php

namespace App\Presenters;

use Nette;

use Nette\Application\UI\Form;
use Nette\Security\User;


class AdminPresenter extends Nette\Application\UI\Presenter
{
	/** @var Nette\Database\Context */
	private $database;
	
	public function __construct(Nette\Database\Context $database){
		$this->database = $database;
	}
	
	
	public function renderDefault($page){
		$user = $this->getUser();
		if(!$user->isLoggedIn()){
			$this->redirect('Sign:in');
		}
		else {$user->setExpiration('30 minutes');}
		
		if($page=="") $page = "HOME";
		$this->template->actualPage = $page;
		$this->template->pageList = $this->database->table('page')->select('name')->order('rank ASC');
		$this->template->pageInfo = $this->database->table('page')->where('name', $page)->fetch();
		$this->template->pageSections = $this->database->table('section')
									->where('page',$page)
									->where('active', '1')
									->order('rank ASC');
		$this->template->username = $user->getIdentity()->username;
	
	}
	
	
	
	
}
