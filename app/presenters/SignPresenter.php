<?php

namespace App\Presenters;

use Nette;

use Nette\Application\UI\Form;


class SignPresenter extends Nette\Application\UI\Presenter
{
	/** @var Nette\Database\Context */
	private $database;
	
	public function __construct(Nette\Database\Context $database){
		$this->database = $database;
	}
	
	protected function createComponentSignInForm()
    {
        $form = new Form;
        $form->addText('username', 'Uživatelské jméno:')
            ->setRequired('Prosím vyplňte své uživatelské jméno.');

        $form->addPassword('password', 'Heslo:')
            ->setRequired('Prosím vyplňte své heslo.');

        $form->addSubmit('send', 'Přihlásit');

        $form->onSuccess[] = [$this, 'signInFormSucceeded'];
        return $form;
    }
	
	public function signInFormSucceeded(Form $form, Nette\Utils\ArrayHash $values)
{
    try {
        $this->getUser()->login($values->username, $values->password);
        $this->redirect('Admin:');

    } catch (Nette\Security\AuthenticationException $e) {
        $form->addError('Nesprávné přihlašovací jméno nebo heslo.');
    }
}
	
	public function renderDefault($page){
		if($this->getUser()->isLoggedIn()){
			$this->setLayout('login');
		}
		else{
			
		}
	}
	
	public function renderIn(){
		
	}
	
	public function renderReg(){
		
		$this->template->password = Nette\Security\Passwords::hash("kaktus135");
	}

	
	
	
}
