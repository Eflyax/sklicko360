<?php

namespace App\Presenters;

use Nette;

use Nette\Application\UI\Form;
use Nette\Security\User;


class ActionPresenter extends Nette\Application\UI\Presenter
{
	/** @var Nette\Database\Context */
	private $database;
	
	public function __construct(Nette\Database\Context $database){
		$this->database = $database;
	}
	
	
	public function renderDefault($page){
		$user = $this->getUser();
		if(!$user->isLoggedIn()){
			$this->redirect('Sign:in');
		}
		else {$user->setExpiration('30 minutes');} 
	
	}
	
	public function RenderMove($ident, $alg){
		if ($this->isAjax()) {
			$user = $this->getUser();
			if(!$user->isLoggedIn()){
				$this->presenter->payload->message = "unauthorized";
			}
			else {
				$user->setExpiration('30 minutes');
				
				$count = $this->database->table('section')
				->where('id', $ident) // must be called before update()
				->update(['align' => $alg, 'rank' => $pos]);
				
				$this->presenter->payload->message = "succeed";
			}
			$this->presenter->sendPayload();
		}
	}
	public function RenderRankchange($direction, $ident){
		if ($this->isAjax()) {
			$user = $this->getUser();
			if(!$user->isLoggedIn()){
				$this->presenter->payload->message = "unauthorized";
			}
			else {
				$user->setExpiration('30 minutes');
				$act = $this->database->table('section')->select('*')->where('id', $ident)->fetch();
				$nw = $this->database->table('section')->select('*')->where('page', $act->page)
															->where('rank '.($direction=="top"?'>':'<').' ?', $act->rank)
															->order('rank '($direction=="top"?'DESC':'ASC'));
															
				$count1 = $this->database->table('section')
				->where('id', $ident) // must be called before update()
				->update(['rank' => $nw->rank]);
				
				$count2 = $this->database->table('section')
				->where('id', $nw->id) // must be called before update()
				->update(['rank' => $act->rank]);
				
				$this->presenter->payload->message = "succeed";
			}
			$this->presenter->sendPayload();
		}
	}
	
	
	
	
}
