<?php

namespace App;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;


class RouterFactory
{
    use Nette\StaticClass;

    /**
     * @return Nette\Application\IRouter
     */
    public static function createRouter(Nette\Database\Context $database)
    {
        $router = new RouteList;

        $pageList = $database->table('page')->select('name')->order('rank ASC');
        foreach ($pageList as $page) {
            $router[] = new Route(str_replace(' ', '+', $page->name),
                [
                    'presenter' => 'Page',
                    'action' => 'show',
                    'page' => $page->name,
                ]);
        }

        $router[] = new Route('<presenter>/<action>/[/<id>]', 'Page:default');

        return $router;
    }
}
